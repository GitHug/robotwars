import { expect } from 'chai';
import { setArena, getArena } from './arena';
import { Coordinates } from './classes';

describe('arena', () => {
  it('should be able to set and get the arena', () => {
    const arena = new Coordinates(5, 5);

    setArena(arena);

    expect(getArena()).to.equal(arena);
  });
});
