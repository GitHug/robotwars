import { expect } from 'chai';
import { Robot, Coordinates } from './classes';
import { Instruction, Direction } from './enums';
import { setArena } from './arena';

describe('classes', () => {
  describe('Robot', () => {
    before(() => {
      setArena(new Coordinates(5, 5));
    });

    it('should be able to rotate left', () => {
      const robot:Robot = new Robot(new Coordinates(0, 0), Direction.N, [Instruction.L]);

      robot.execute();

      expect(robot.direction).to.equal(Direction.W);
    });

    it('should be able to rotate left', () => {
      const robot:Robot = new Robot(new Coordinates(0, 0), Direction.N, [Instruction.R]);

      robot.execute();

      expect(robot.direction).to.equal(Direction.E);
    });

    it('should be able to rotate 360 degrees left', () => {
      const robot:Robot = new Robot(
        new Coordinates(0, 0), Direction.N,
        [Instruction.L, Instruction.L, Instruction.L, Instruction.L],
      );

      robot.execute();

      expect(robot.direction).to.equal(Direction.N);
    });

    it('should be able to rotate 360 degrees right', () => {
      const robot:Robot = new Robot(
        new Coordinates(0, 0), Direction.N,
        [Instruction.R, Instruction.R, Instruction.R, Instruction.R],
      );

      robot.execute();

      expect(robot.direction).to.equal(Direction.N);
    });

    it('should be able to move forward', () => {
      const robot:Robot = new Robot(new Coordinates(0, 0), Direction.N, [Instruction.M]);

      robot.execute();

      expect(robot.coordinates).to.deep.equal({ x: 0, y: 1 });
    });

    it('should not be able to move outside the arena', () => {
      const robot:Robot = new Robot(
        new Coordinates(0, 0), Direction.S,
        [Instruction.M, Instruction.M, Instruction.M],
      );

      robot.execute();

      expect(robot.coordinates).to.deep.equal({ x: 0, y: 0 });
    });

    it('should be able to complete complex instructions', () => {
      const instructions:Instruction[] = 'LMLMLMLMMMMRMMRMRRM'.split('').map((instr) => {
        switch (instr) {
          case 'L':
            return Instruction.L;
          case 'R':
            return Instruction.R;
          case 'M':
            return Instruction.M;
          default:
            throw new Error('This should not happen');
        }
      });

      const robot:Robot = new Robot(new Coordinates(1, 2), Direction.N, instructions);
      robot.execute();

      expect(robot.coordinates).to.deep.equal({ x: 3, y: 5 });
      expect(robot.direction).to.equal(Direction.N);
    });
  });
});
