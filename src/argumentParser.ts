import { Robot, Coordinates } from './classes';
import { Direction, Instruction } from './enums';
import { arenaRegexp, robotLocationRegexp } from './validator';

const parseArenaCoordinates = (arg:string):Coordinates => {
  const match:RegExpExecArray = arenaRegexp.exec(arg);

  const coordinates = new Coordinates(
    Number.parseInt(match[1], 10),
    Number.parseInt(match[2], 10),
  );

  return coordinates;
};

const parseRobotCoordinatesAndInstructions = (robotArgs:string, instructionArgs:string):Robot => {
  const match:RegExpExecArray = robotLocationRegexp.exec(robotArgs);

  const coordinates = new Coordinates(
    Number.parseInt(match[1], 10),
    Number.parseInt(match[2], 10),
  );

  let direction:Direction;
  switch (match[3]) {
    case 'N':
      direction = Direction.N;
      break;
    case 'E':
      direction = Direction.E;
      break;
    case 'S':
      direction = Direction.S;
      break;
    case 'W':
      direction = Direction.W;
      break;
    default:
      throw new Error('Unknown direction');
  }

  const instructions:Instruction[] = instructionArgs.split('').map(instr => Instruction[instr]);

  return new Robot(coordinates, direction, instructions);
};

const parse = (args:string[]):{arena:Coordinates, robots:Robot[]} => {
  const arena:Coordinates = parseArenaCoordinates(args[0]);

  const robots:Robot[] = [];

  let index:number = 1;
  while (index < args.length) {
    robots.push(parseRobotCoordinatesAndInstructions(args[index], args[index + 1]));
    index += 2;
  }

  return { arena, robots };
};

export default parse;
