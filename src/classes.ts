import { Direction, Instruction, directions, directionNames } from './enums';
import { getArena } from './arena';

export class Coordinates {
  public x: number
  public y: number

  constructor(x:number, y:number) {
    this.x = x;
    this.y = y;
  }
}

export class Robot {
  public coordinates:Coordinates
  public direction:Direction
  public instructions:Instruction[]

  constructor(coordinates:Coordinates, direction:Direction, instructions: Instruction[]) {
    this.coordinates = coordinates;
    this.direction = direction;
    this.instructions = instructions;
  }

  public rotate = (instruction: Instruction) => {
    const newDirection:number =
      (this.direction + instruction) < 0 ? 3 : (this.direction + instruction) % 4;
    this.direction = directions[newDirection];
  };

  public move = () => {
    const { x, y }:{x:number, y:number} = this.coordinates;
    const arena = getArena();

    switch (this.direction) {
      case Direction.N:
        this.coordinates.y = Math.min(y + 1, arena.y);
        break;
      case Direction.E:
        this.coordinates.x = Math.min(x + 1, arena.x);
        break;
      case Direction.S:
        this.coordinates.y = Math.max(y - 1, 0);
        break;
      case Direction.W:
        this.coordinates.x = Math.max(x - 1, 0);
        break;
      default:
        throw new Error('Unnown direction');
    }
  };

  public execute = () => {
    this.instructions.forEach((instruction) => {
      switch (instruction) {
        case Instruction.L:
        case Instruction.R:
          this.rotate(instruction);
          break;
        case Instruction.M:
          this.move();
          break;
        default:
          throw new Error('Unknown instruction');
      }
    });
  }

  public toString = () => `${this.coordinates.x} ${this.coordinates.y} ${directionNames[this.direction]}`
}

