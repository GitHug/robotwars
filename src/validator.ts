export const arenaRegexp:RegExp = /^([\d]+)\s+([\d]+)$/;
export const robotLocationRegexp:RegExp = /^([\d]+)\s+([\d]+)\s+([NESW])$/;
export const instructionsRegexp:RegExp = /[LRM]+/;

let arenaCoordinates: {x:number, y:number};

const validateArenaCoordinates = (arg:string):void => {
  if (!arenaRegexp.test(arg)) {
    throw new Error(`Invalid arena coordinates '${arg}'. Try again.`);
  }

  const match:RegExpExecArray = arenaRegexp.exec(arg);

  arenaCoordinates = {
    x: Number.parseInt(match[1], 10),
    y: Number.parseInt(match[2], 10),
  };
};

const validateRobotCoordinates = (arg:string):void => {
  if (!robotLocationRegexp.test(arg)) throw new Error(`Invalid robot coordinates '${arg}'. Try again.`);

  const match:RegExpExecArray = robotLocationRegexp.exec(arg);

  const coordinates:{x:number, y:number} = {
    x: Number.parseInt(match[1], 10),
    y: Number.parseInt(match[2], 10),
  };

  if (coordinates.x > arenaCoordinates.x || coordinates.y > arenaCoordinates.y) {
    throw new Error(`Robot is out of bounds at ${JSON.stringify(coordinates)}`);
  }
};
const validateInstructions = (arg:string):void => {
  if (!instructionsRegexp.test(arg)) throw new Error(`Invalid instructions '${arg}'. Try again.`);
};

const isArenaCoordinates = (index:number):boolean => index === 0;
const isRobotCoordinates = (index:number):boolean => index % 2 === 1;

const validate = (arg:string, index:number):void => {
  if (isArenaCoordinates(index)) {
    validateArenaCoordinates(arg);
  } else if (isRobotCoordinates(index)) {
    validateRobotCoordinates(arg);
  } else {
    validateInstructions(arg);
  }
};

export const validateNumerOfArguments = (args:string[]):void => {
  if (args.length < 3 && args.length % 2 !== 1) throw new Error('Invalid number of arguments');
};

export default validate;
