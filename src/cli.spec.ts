import * as chai from 'chai';
import * as cap from 'chai-as-promised';
import cli, { rl } from './cli';
import { Direction, Instruction } from './enums';

chai.use(cap);
const { expect } = chai;

describe('cli', () => {
  it('should resolve valid arguments', (done) => {
    process.on('unhandledRejection', (e) => { throw e; });
    const ret = cli();
    rl.write('5 5\r\n');
    rl.write('1 2 N\r\n');
    rl.write('LMLMLMLMM\r\n');
    rl.write('3 3 E\r\n');
    rl.write('MMRMMRMRRM\r\n');
    rl.close();

    ret.then((value) => {
      const { arena } = value;
      expect(arena).to.deep.equal({
        x: 5, y: 5,
      });

      expect(value.robots.length).to.equal(2);

      const firstRobot = value.robots[0];
      expect(firstRobot.coordinates).to.deep.equal({ x: 1, y: 2 });
      expect(firstRobot.direction).to.equal(Direction.N);
      expect(firstRobot.instructions).to.deep.equal([
        Instruction.L,
        Instruction.M,
        Instruction.L,
        Instruction.M,
        Instruction.L,
        Instruction.M,
        Instruction.L,
        Instruction.M,
        Instruction.M,
      ]);

      const secondRobot = value.robots[1];
      expect(secondRobot.coordinates).to.deep.equal({ x: 3, y: 3 });
      expect(secondRobot.direction).to.equal(Direction.E);
      expect(secondRobot.instructions).to.deep.equal([
        Instruction.M,
        Instruction.M,
        Instruction.R,
        Instruction.M,
        Instruction.M,
        Instruction.R,
        Instruction.M,
        Instruction.R,
        Instruction.R,
        Instruction.M,
      ]);

      return done();
    }).catch(e => done(e));
  });
});
