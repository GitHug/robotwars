import { expect } from 'chai';
import parser from './argumentParser';
import { Instruction, Direction } from './enums';

describe('argumentParser', () => {
  it('should parse elements', () => {
    const args:string[] = [
      '5 5',
      '1 2 N',
      'LMLMLMLMM',
      '3 3 E',
      'MMRMMRMRRM',
    ];

    const ret = parser(args);

    const { arena } = ret;
    expect(arena).to.deep.equal({
      x: 5, y: 5,
    });

    expect(ret.robots.length).to.equal(2);

    const firstRobot = ret.robots[0];
    expect(firstRobot.coordinates).to.deep.equal({ x: 1, y: 2 });
    expect(firstRobot.direction).to.equal(Direction.N);
    expect(firstRobot.instructions).to.deep.equal([
      Instruction.L,
      Instruction.M,
      Instruction.L,
      Instruction.M,
      Instruction.L,
      Instruction.M,
      Instruction.L,
      Instruction.M,
      Instruction.M,
    ]);

    const secondRobot = ret.robots[1];
    expect(secondRobot.coordinates).to.deep.equal({ x: 3, y: 3 });
    expect(secondRobot.direction).to.equal(Direction.E);
    expect(secondRobot.instructions).to.deep.equal([
      Instruction.M,
      Instruction.M,
      Instruction.R,
      Instruction.M,
      Instruction.M,
      Instruction.R,
      Instruction.M,
      Instruction.R,
      Instruction.R,
      Instruction.M,
    ]);
  });
});
