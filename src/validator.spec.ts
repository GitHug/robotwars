import { expect } from 'chai';
import validate, { validateNumerOfArguments } from './validator';

describe('validator', () => {
  describe('validate arena coordinates', () => {
    it('should throw an error for too few coordinates', () => {
      expect(() => validate('1', 0)).to.throw();
    });

    it('should throw an error for too many coordinates', () => {
      expect(() => validate('1 1 1', 0)).to.throw();
    });

    it('should throw an error for non-numeric coordinates', () => {
      expect(() => validate('a b', 0)).to.throw();
    });

    it('should throw an error for empty coordinates', () => {
      expect(() => validate('', 0)).to.throw();
    });

    it('should throw an error for negative coordinates', () => {
      expect(() => validate('-1 1', 0)).to.throw();
    });

    it('should not throw an error for correct coordinates', () => {
      expect(() => validate('1 1', 0)).not.to.throw();
    });
  });

  describe('validate robot location', () => {
    it('should throw an error for too few coordinates', () => {
      expect(() => validate('1 E', 1)).to.throw();
    });

    it('should throw an error for too many coordinates', () => {
      expect(() => validate('1 1 1 E', 1)).to.throw();
    });

    it('should throw an error for missing direction', () => {
      expect(() => validate('1 1', 1)).to.throw();
    });

    it('should throw an error for empty string', () => {
      expect(() => validate('', 1)).to.throw();
    });

    it('should throw an error for incorrect direction', () => {
      expect(() => validate('1 1 A', 1)).to.throw();
    });

    it('should throw an error if outside arena', () => {
      expect(() => validate('1 2 E', 1)).to.throw();
    });

    it('should not throw any error if the coordinates and direction are correct', () => {
      expect(() => validate('1 1 E', 1)).not.to.throw();
    });
  });

  describe('validate instructions', () => {
    it('should throw an error for empty string', () => {
      expect(() => validate('', 2)).to.throw();
    });

    it('should throw an error for incorrect instruction', () => {
      expect(() => validate('A', 2)).to.throw();
    });

    it('should not throw an error for a correct instruction', () => {
      expect(() => validate('M', 2)).not.to.throw();
    });
  });
});
