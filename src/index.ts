import cli from './cli';
import { Robot } from './classes';
import { setArena } from './arena';

cli().then((args) => {
  setArena(args.arena);
  const { robots }: {robots:Robot[]} = args;

  robots.forEach((robot:Robot) => {
    robot.execute();
    console.log(robot.toString());
  });
});

