export enum Direction {
  N = 0,
  E = 1,
  S = 2,
  W = 3
}

export enum Instruction {
  L = -1,
  R = 1,
  M = 0
}

export const directions:Direction[] = [Direction.N, Direction.E, Direction.S, Direction.W];
export const directionNames:string[] = ['N', 'E', 'S', 'W'];
