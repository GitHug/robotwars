import { Coordinates } from './classes';

let arena:Coordinates;
export const setArena = (ar:Coordinates):void => {
  arena = ar;
};
export const getArena = ():Coordinates => arena;
