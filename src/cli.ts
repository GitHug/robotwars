import * as readline from 'readline';
import validate, { validateNumerOfArguments } from './validator';
import parse from './argumentParser';
import { Coordinates, Robot } from './classes';

export const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

export default ():Promise<{arena:Coordinates, robots:Robot[]}> => new Promise((resolve, reject) => {
  console.log('Ready to rumble?');

  rl.prompt();

  const args:string[] = [];

  rl.on('line', (line:string) => {
    if (line && line.trim().length > 0) {
      try {
        validate(line, args.length);
      } catch (e) {
        rl.close();
        return reject(e.message);
      }

      return args.push(line);
    }
    return rl.close();
  }).on('close', () => {
    try {
      validateNumerOfArguments(args);
    } catch (e) {
      return reject(e.message);
    }
    return resolve(parse(args));
  });
});
