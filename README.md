# RobotWars

## Installation

```
yarn install
```

## Input
The first line of input is the upper-right coordinates of the arena, the lower-left coordinates are assumed to be (0, 0).

The rest of the input is information pertaining to the robots that have been deployed. Each robot has two lines
of input - the first gives the robot’s position and the second is a series of instructions telling the robot how to
move within the arena.

The position is made up of two integers and a letter separated by spaces, corresponding to the x and y
coordinates and the robot’s orientation. Each robot will finish moving sequentially, which means that the
second robot won’t start to move until the first one has finished moving.

End of input is indicated by an empty line.

## Output
The output for each robot should be its final coordinates and heading.

## How to run
```
yarn start
```

### Example
```
// Init
$ yarn start

// Input
Ready to rumble?
> 5 5
1 2 N
LMLMLMLMM
3 3 E
MMRMMRMRRM

// Output
1 3 N
5 1 E
```

